//wszystko dla modułu nrf
#include <SPI.h>
#include <RH_NRF24.h>
#define CE 9 //digital 9
#define CSN 10 //digital 10
RH_NRF24 nrf24(CE, CSN);

//wszystko dla modułu joysticka
#include <myJoystick.h>
#define xPin A6 // analog 6
#define yPin A7 // analog 7
#define cPin 8 // digital 8
myJoystick joystick(xPin,yPin,cPin);

void setup()
{
  Serial.begin(9600);
  while (!Serial)
  ; //czekaj aż serial port zostanie zainicjowany, potrzebne tylko dla Arduino Leonardo
  if (!nrf24.init())
    Serial.println("init failed");

  //niezbędne ustawienia dla modułu nrf24, wybieram kanał 1
  if (!nrf24.setChannel(1))
    Serial.println("setChannel failed");
  if (!nrf24.setRF(RH_NRF24::DataRate2Mbps, RH_NRF24::TransmitPower0dBm))
    Serial.println("setRF failed");

  //inicjalizacja joysticka
  joystick.init();
}

void loop()
{
  //wysyłanie statusu joysticka
  nrf24.send(joystick.getState(), joystick.getStateSize());
  //odczkaj aż status zostanie wysłany
  nrf24.waitPacketSent();
}
