//Autor: Cezary Wernik

#include <myJoystick.h>

myJoystick::myJoystick(int _X_pin, int _Y_pin, int _C_pin )
{
    X=_X_pin;
    Y=_Y_pin;
    C=_C_pin;
  }

  void myJoystick::init()
  {
    initClickPin();

    //ustawienie źródła referencyjnego dla ADC jako zewnętrzne
    //dzięki czemu przy użyciu 3.3V zachowujemy zakres odczytu od 0 do 1023
    analogReference(EXTERNAL);

    //domyslny stan joysticka
    //roboczo przyjąłem koncepcję 5 stanów joysticka opisywanych od a do c {a,b,c}
    //a na osi X i Y oznacza minimum, na przycisku stan nie wciśnięcia
    //b na osi X i Y oznacza pozycję neutralną, na przycisku nie wystepuje
    //c na osi X i Y oznacza mksimum, na przycisku stan wciśnięcia
    STATE[0]='[';
    STATE[1]='b';
    STATE[2]='b';
    STATE[3]='c';
    STATE[4]=']';
    STATE[5]='\0';
  }

  void myJoystick::initClickPin()
  {
    //ustawienie pinu jako wejściowy
    pinMode(C,INPUT);
    //ustawienie pull-up dla wybranego wejściowego pinu C (patrz http://arduino.cc/en/Tutorial/DigitalPins)
    digitalWrite(C,HIGH);
  }

  int myJoystick::mapAxis(int axis)
  {
    return (int)(map(axis, sensorMin, sensorMax, sensorMinMap, sensorMaxMap));
  }

  char myJoystick::getAxis(int pin)
  {
    int val = mapAxis(analogRead(pin));
    //przeskalowaną wartośc od 0 do 255 mapuję na stany a, b lub c
    if(val<100)
      return (char)'a';
    else if(val>=100 && val<=150)
      return (char)'b';
      else return (char)'c';
  }

  char myJoystick::getClick()
  {
    return (char)((digitalRead(C)==HIGH)?('a'):('c')); // będzie HIGH (1) gdy niewciśnięte i LOW (0) gdy wciśnięte
  }

  uint8_t *myJoystick::getState()
  {
    STATE[0]='[';
    STATE[1]=getAxis(X);
    STATE[2]=getAxis(Y);
    STATE[3]=getClick();
    STATE[4]=']';
    STATE[5]='\0';
    return STATE;
  }

  uint8_t myJoystick::getStateSize()
  {
    return sizeof(STATE);
  }
