//Autor: Cezary Wernik

#ifndef myJoystick_h
#define myJoystick_h

#include <Arduino.h>
#include <stdint.h>

class myJoystick
{
private:
  int sensorMin = 0;     // minimalna wartośc odczytana z pinu analogowego
  int sensorMax = 1023;  // maksymalna wartośc odczytana z pinu analogowego
  int sensorMinMap = 255;// minimalna wartośc do mapowania stanu
  int sensorMaxMap = 0;  // maksymalna wartośc do mapowania stanu
  int X,Y,C;
  uint8_t STATE[6]; // { '[', 'X', 'Y', 'C', ']', '\0' };

public:
  myJoystick(int _X_pin, int _Y_pin, int _C_pin );
  void init();
  void initClickPin();
  int mapAxis(int axis);
  char getAxis(int pin);
  char getClick();
  uint8_t *getState();
  uint8_t getStateSize();
};

#endif
